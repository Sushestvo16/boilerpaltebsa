const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query

    if (res.statusCode == 404) {
        res.json({
            error: true,
            message: `${res.error}` ? `${res.error}` : 'Not found, status code: 404'
        })
    } else if (res.statusCode == 400) {
        res.json({
            error: true,
            message: `${res.error}` ? `${res.error}` : 'Bad Request, status code: 400'
        })
    }
}

exports.responseMiddleware = responseMiddleware;