const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { name, power, defense, health = 100 } = req.body;

    if (!name || !power || !defense) {
        return res.status(400).json({ msg: "Please fill in all fields." });
    }

    if (Number(defense) > 10 || Number(defense) < 1) {
        return res.status(400).json({ msg: "Defense must be numbers from one to ten." })
    }
    if (power > 100 || power < 1) {
        return res.status(400).json({ msg: "Power must be numbers from one to hundred." })
    }

    if (health < 80 || health > 120) {
        return res.status(400).json({ msg: "Health must be numbers from eighty to one hundred twenty." })
    }
    if (FighterService.search({ name })) {
        return res.status(400).json({ msg: "This fighter name already exist. Please enter a other name" })
    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const { name, power, defense, health } = req.body;

    if (defense > 10 && defense < 1) {
        return res.status(400).json({ msg: "Defense must be numbers from one to ten." })
    }
    if (power > 100 || power < 1) {
        return res.status(400).json({ msg: "Defense must be numbers from one to hundred." })
    }
    if (health < 80 || health > 120) {
        return res.status(400).json({ msg: "Health must be numbers from eighty to one hundred twenty." })
    }



    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;