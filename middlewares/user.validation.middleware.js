const { user } = require('../models/user');

const UserService = require('../services/userService');

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function validateNumber(value) {
    if (/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
        return Number(value);
    return false
}


const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const { firstName, lastName, email, phoneNumber, password } = req.body;

    if (!firstName || !lastName || !email || !phoneNumber || !password) {
        return res.status(400).json({ msg: "Please fill in all fields." })
    }


    if (UserService.search({ email })) {
        return res.status(400).json({ msg: "This email already exists." })
    }
    if (UserService.search({ phoneNumber })) {
        return res.status(400).json({ msg: "User with this phone number already exist. Please enter another phone number." })
    }
    if (phoneNumber.length < 9 || phoneNumber.length > 9) {
        return res.status(400).json({ msg: "Phone number must be at least 9 characters." })
    }

    if (password.length < 6) {
        return res.status(400).json({ msg: "Password must be at least 6 characters." })
    }
    if (!validateEmail(email)) {
        return res.status(400).json({ msg: "Email is not valid." })
    }

    if (!validateNumber(phoneNumber)) {
        return res.status(400).json({ msg: "Phone number is not valid. Please enter valid number." })
    }

    next();
}
const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const { firstName, lastName, email, phoneNumber, password } = req.body;
    console.log(req.body)
    if (password.length < 6) {
        return res.status(400).json({ msg: "Password must be at least 6 characters." })
    }
    if (phoneNumber.length < 9) {
        return res.status(400).json({ msg: "Phone number must be at least 9 characters." })
    }
    if (!validateEmail(email)) {
        return res.status(400).json({ msg: "Email is not valid." })
    }
    if (!validateNumber(phoneNumber)) {
        return res.status(400).json({ msg: "Phone number is not valid." })
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;