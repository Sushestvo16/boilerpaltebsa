const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('/', createFighterValid, (req, res, next) => {
    try {
        const { name, power, defense, health = 100 } = req.body;
        const newFighter = FighterService.create({ name, power, defense, health });
        console.log(newFighter);
        if (newFighter) {
            res.status(201).json({
                name: newFighter.name,
                power: newFighter.power,
                defense: newFighter.defense,
                health: newFighter.health
            })
        } else {
            res.status(400)
            throw new Error('Invalid user data')
        }
    } catch (error) {
        res.error = error;

    } finally {
        next();
    }
}, responseMiddleware)
router.get('/', (req, res, next) => {
    try {
        const fighters = FighterService.getAllFighters()
        if (fighters) {
            res.json(fighters)
        } else {
            res.status(404)
            throw new Error('Fighters not found')
        }

    } catch (error) {
        res.error = error;
    } finally {
        next();

    }
}, responseMiddleware)
router.get('/:id', (req, res, next) => {

    try {
        const id = req.params.id;
        console.log(id);
        const fighter = FighterService.search({ id });;

        console.log('fighter', fighter)
        if (fighter) {
            res.status(200);
            res.json(fighter);
        } else {
            res.status(404);
            throw new Error('Fighter not found')
        }

    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware)



router.delete('/:id', (req, res, next) => {
    try {
        const id = req.params.id;
        const fighterFinded = FighterService.search({ id })

        if (fighterFinded) {

            FighterService.delete(fighterFinded.id);
            res.json({ message: 'Fighter removed' })
        } else {
            res.status(404)
            throw new Error('Fighter not found')
        }

    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware)
router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        const id = req.params.id;
        const fighter = FighterService.search({ id });

        const info = {
            name: req.body.name,
            power: req.body.power,
            defense: req.body.defense,
            health: req.body.health || "100"
        }
        if (fighter) {
            const updateFighter = FighterService.update(id, info);

            res.json({
                name: updateFighter.name,
                power: updateFighter.power,
                defense: updateFighter.defense,
            })
            updateFighter.save();
        } else {
            res.status(404)
            throw new Error('Fighter not found')
        }
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware)




module.exports = router;