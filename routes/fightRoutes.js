const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');



const router = Router();

// OPTIONAL TODO: Implement route controller for fights
router.post('/', (req, res, next) => {
    try {
        const { fighter1, fighter2, log } = req.body;
        const fight = FightService.create({ fighter1, fighter2, log });
        if (fight) {
            res.status(201).json({
                fighter1: fight.fighter1,
                fighter2: fight.fighter2,
                log: fight.log
            })
        }

    } catch (error) {

    } finally {
        next();
    }
})


module.exports = router;