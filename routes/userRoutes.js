const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();


// TODO: Implement route controllers for user
router.post('/', createUserValid, (req, res, next) => {
    try {
        const { firstName, lastName, email, phoneNumber, password } = req.body;

        const userExists = UserService.search({ email });
        if (userExists) {
            res.status(400)
            throw new Error('user alredy exists')

        }
        const user = UserService.create({ firstName, lastName, email, phoneNumber: "+380" + phoneNumber, password });

        if (user) {
            res.status(201).json({
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                phoneNumber: user.phoneNumber,

            })
        } else {
            res.status(400)
            throw new Error('Invalid user data')
        }
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware)

router.get('/', (req, res, next) => {
    try {
        const users = UserService.getUsers();
        if (users) {
            res.json(users);
        } else {
            res.status(404);
            throw new Error('Users not found')
        }

    } catch (error) {
        res.error = error;
    } finally {
        next();

    }
}, responseMiddleware)

router.get(`/:id`, (req, res, next) => {
    try {
        const id = req.params.id;
        const user = UserService.search({ id });
        if (user) {
            res.status(200)
            res.json(user);
        } else {
            res.status(404);
            throw new Error('User not found')
        }

    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware);



router.put('/:id', updateUserValid, (req, res, next) => {
    try {

        const id = req.params.id;

        const user = UserService.search({ id });
        const info = {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            phoneNumber: req.body.phoneNumber,
            password: req.body.password
        }

        if (user) {
            UserService.update(id, info);


            res.json({

                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                phoneNumber: "+380" + user.phoneNumber,
                password: user.password
            })
            user.save();
            console.log(user)
        } else {
            res.status(404)
            throw new Error('User not found')
        }
    } catch (error) {
        res.error = error;
    } finally {
        next();
    }

}, responseMiddleware)

router.delete('/:id', (req, res, next) => {
    try {
        const id = req.params.id;

        const userFinded = UserService.search({ id })

        if (userFinded) {
            UserService.delete(userFinded.id);
            res.json({ message: 'User removed' })

        } else {
            res.status(404)
            throw new Error('User not found')
        }

    } catch (error) {
        res.error = error;
    } finally {
        next();
    }
}, responseMiddleware)




module.exports = router;