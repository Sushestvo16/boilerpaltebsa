const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const { email, password } = req.body;
        const data = AuthService.login({ email });
        console.log('req.body', req.body);
        console.log(data);
        if (data && password === data.password) {
            res.json({
                firstName: data.firstName,
                lastName: data.lastName,
                email: data.email,
                phoneNumber: data.phoneNumber
            })
        }
        else {
            res.status(401)
            throw new Error('Invalid email or password')
        }




    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;