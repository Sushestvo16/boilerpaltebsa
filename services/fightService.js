const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights

    create(fight) {
        let created = FightRepository.create(fight);
        if (!created) {
            throw Error('Fight not created');
        }
        return fight;
    }
    getAllFight() {
        const fight = FightRepository.getAll();
        if (!fight) {
            return null;
        }
        return fight;
    }
    search(search) {
        const fight = FightRepository.getOne(search);
        if (!fight) {
            return null;
        }
        return fight;
    }
    update(id, info) {
        const result = FightRepository.update(id, info);
        if (!result) {
            throw Error('Fight not found');
        }
        return result;
    }
    remove(id) {
        const fight = FightRepository.remove(id);
        if (!fight) {
            throw Error('Fight not found');
        }
        return fight;
    }
    getId() {
        const id = FightRepository.generateId()
        if (!id) {
            throw Error('Fight not found');
        }
        return id;
    }
}

module.exports = new FightersService();