const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    create(fighter) {
        let created = FighterRepository.create(fighter);
        if (!created) {
            throw Error('Fighter not created');
        }
        return created;
    }
    getAllFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            return null;
        }
        return fighters;
    }
    search(search) {
        const fighter = FighterRepository.getOne(search);
        if (!fighter) {
            return null;
        }
        return fighter;
    }
    update(id, info) {
        const result = FighterRepository.update(id, info);
        if (!result) {
            throw Error('Fighter not found');
        }
        return result;
    }
    delete(id) {
        const fighter = FighterRepository.delete(id);
        if (!fighter) {
            throw Error('Fighter not found');
        }
        return fighter;
    }
    getId() {
        const id = FighterRepository.generateId()
        if (!id) {
            throw Error('Fighter not found');
        }
        return id;
    }
}

module.exports = new FighterService();